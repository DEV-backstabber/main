package us.hhbz.fyp.communicators;

import java.util.Random;

public class DummyCommunicator {
	private String lastData;
	private String device;
	private Random random;
	public DummyCommunicator(String device) {
		this.device=device;
		System.out.println("Enabling Serial Port for device: "+device);
		random=new Random();
	}
	
	public void sendData(String data) {
		System.out.println("Sent data: "+data+" on device: "+device);
		lastData=data+":"+(random.nextInt(500)-100);
	}
	public String getLastData() {
		return lastData;
	}
}
