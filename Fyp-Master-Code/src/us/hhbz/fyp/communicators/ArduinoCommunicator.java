package us.hhbz.fyp.communicators;

import java.io.IOException;

import com.pi4j.io.serial.Baud;
import com.pi4j.io.serial.DataBits;
import com.pi4j.io.serial.FlowControl;
import com.pi4j.io.serial.Parity;
import com.pi4j.io.serial.Serial;
import com.pi4j.io.serial.SerialConfig;
import com.pi4j.io.serial.SerialDataEvent;
import com.pi4j.io.serial.SerialDataEventListener;
import com.pi4j.io.serial.SerialFactory;
import com.pi4j.io.serial.StopBits;

public class ArduinoCommunicator {
	private Serial port;
	private String lastData;
	public ArduinoCommunicator(String device) {
		this.port=SerialFactory.createInstance();
		
		SerialConfig config=new SerialConfig();
		config.device(device);
		config.baud(Baud._9600);
		config.dataBits(DataBits._8);
		config.parity(Parity.NONE);
		config.stopBits(StopBits._1);
		config.flowControl(FlowControl.NONE);
		try {
			port.open(config);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		port.addListener(new SerialDataEventListener() {
			
			@Override
			public void dataReceived(SerialDataEvent event) {
				try {
					lastData=event.getAsciiString();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void sendData(String data) {
		try {
			port.write(data);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
		}
	}
	public String getLastData() {
		return lastData;
	}
}
