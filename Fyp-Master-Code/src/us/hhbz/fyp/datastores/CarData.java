package us.hhbz.fyp.datastores;

import java.util.HashMap;
import java.util.Map;

import us.hhbz.fyp.enums.DataTypes;

public class CarData {
	private Map<DataTypes, Integer> data=new HashMap<DataTypes, Integer>();
	public CarData() {
		for(DataTypes type:DataTypes.values()) {
			data.put(type, -1);
		}
	}
	public void setData(DataTypes type,int data) {
		this.data.put(type, data);
	}
	public int getData(DataTypes type) {
		return this.data.get(type);
	}
}
