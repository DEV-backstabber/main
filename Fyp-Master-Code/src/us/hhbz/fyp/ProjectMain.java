package us.hhbz.fyp;


import java.io.InputStream;


public class ProjectMain {

	public static void main(String[] args) {  
		new MainObject().onStart();
	}  
	
	public static InputStream getResource(String fileName) {
		
		return ProjectMain.class.getResourceAsStream("/media/"+fileName);
	      
	}
}
