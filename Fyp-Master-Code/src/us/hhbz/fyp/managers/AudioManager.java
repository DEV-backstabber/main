package us.hhbz.fyp.managers;

import us.hhbz.fyp.MainObject;
import us.hhbz.fyp.communicators.ArduinoCommunicator;
import us.hhbz.fyp.communicators.DummyCommunicator;
import us.hhbz.fyp.enums.AudioTypes;

public class AudioManager {
	@SuppressWarnings("unused")
	private MainObject main;
	//private ArduinoCommunicator communicator=new ArduinoCommunicator("/dev/ttyACM1");
	private DummyCommunicator communicator=new DummyCommunicator("/dev/ttyACM1");
	//using a dummy communicator far testing
	public AudioManager(MainObject main) {
		this.main=main;
	}
	
	public void volumeUp() {
		communicator.sendData("volume:up");
	}
	public void volumeDown() {
		communicator.sendData("volume:down");
	}
	public void stopPlaying() {
		communicator.sendData("stop");
	}
	public void playSound(AudioTypes type) {
		communicator.sendData("play:"+type.getCommand());
	}
}
