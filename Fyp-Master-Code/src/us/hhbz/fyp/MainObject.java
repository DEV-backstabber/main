package us.hhbz.fyp;

import us.hhbz.fyp.guis.AbstractGui;
import us.hhbz.fyp.guis.DataScreen;
import us.hhbz.fyp.guis.LoadingScreen;
import us.hhbz.fyp.managers.AudioManager;
import us.hhbz.fyp.managers.ConditionManager;
import us.hhbz.fyp.managers.MySqlManager;
import us.hhbz.fyp.managers.ObdManager;

public class MainObject {

	private AbstractGui runningGui;
	private AudioManager audioManager;
	private ObdManager obdManager;
	private ConditionManager conditions;
	private MySqlManager sqlManager;
	
	public void onStart() {
		audioManager=new AudioManager(this);
		obdManager=new ObdManager(this);
		obdManager.setup();
		conditions=new ConditionManager(this);
		conditions.setup();
		
		runningGui=new LoadingScreen();
		runningGui.constructGui();
		runningGui.displayGui();
		for(int progress=0;progress<=100;progress++) {
			runningGui.setProgress(progress);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		runningGui.hideGui();
		runningGui=new DataScreen();
		runningGui.constructGui();
		runningGui.displayGui();
	}
	
	
	public AbstractGui getRunningGui() {
		return this.runningGui;
	}
	public ObdManager getObdManager() {
		return this.obdManager;
	}
	public AudioManager getAudioManager() {
		return this.audioManager;
	}
}
