package us.hhbz.fyp.guis;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import us.hhbz.fyp.enums.DataTypes;

public abstract class AbstractGui {
	protected JFrame frame;
	protected Map<DataTypes, JButton> buttons=new HashMap<DataTypes, JButton>();
	protected Map<String, JTextField> texts=new HashMap<String, JTextField>();
	protected GuiState state;
	protected String guiName;
	/**
	 * get the name of this gui
	 * @return gui name
	 */
	public String getName() {
		return this.guiName;
	}
	/**
	 * get the current state of this gui
	 * @return
	 */
	public GuiState getState() {
		return this.state;
	}

	/**
	 * get a list of all buttons in this gui
	 * @return a map of buttons against their names
	 */
	public Map<DataTypes, JButton> getButtons() {
		return this.buttons;
	}
	/**
	 * get a list of all text fields in this gui
	 * @return a map of buttons against their names
	 */
	public Map<String, JTextField> getTexts() {
		return this.texts;
	}
	/**
	 * Construct this gui from scratch
	 */
	public abstract void constructGui();
	/**
	 * display this gui to the user
	 * will only run if the GuiState is hidden
	 */
	public void displayGui() {
		if(this.frame==null)
			return;
		if(this.state.equals(GuiState.HIDDEN)) {
			this.frame.setVisible(true);
			this.state=GuiState.VISIBLE;
		}
	}

	/**
	 * hide this gui to the user
	 * will only run if the GuiState is visible
	 */
	public void hideGui() {
		if(this.frame==null)
			return;
		if(this.state.equals(GuiState.VISIBLE)) {
			this.frame.setVisible(false);
			this.state=GuiState.HIDDEN;
		}
	}
	
	public abstract void setProgress(int progress);
	public enum GuiState {
		HIDDEN,VISIBLE;
	}
	public static BufferedImage resize(BufferedImage img, int newW, int newH) { 
	    Image tmp = img.getScaledInstance(newW, newH, Image.SCALE_SMOOTH);
	    BufferedImage dimg = new BufferedImage(newW, newH, BufferedImage.TYPE_INT_ARGB);

	    Graphics2D g2d = dimg.createGraphics();
	    g2d.drawImage(tmp, 0, 0, null);
	    g2d.dispose();

	    return dimg;
	}
}
