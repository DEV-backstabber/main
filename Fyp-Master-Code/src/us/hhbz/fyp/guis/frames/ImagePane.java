package us.hhbz.fyp.guis.frames;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class ImagePane extends JPanel {
    private Image image;
    private int height=0;
    private int width=0;
    boolean middle=false;
    public ImagePane(Image image,int width,int height) {
        this.image = image;
        this.height=height;
        this.width=width;
    }
    public ImagePane(Image image,boolean middle) {
        this.image = image;
        this.middle=middle;
    }
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(middle) {
            this.height=getHeight()/2-image.getHeight(this)/2;
            this.width=getWidth()/2-image.getWidth(this)/2;
        }
        g.drawImage(image, width, height, this);
    }
}
